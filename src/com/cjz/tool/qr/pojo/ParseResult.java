package com.cjz.tool.qr.pojo;

public class ParseResult {

	boolean isSuccess;
	String result;

	public ParseResult(boolean isSuccess, String result) {
		super();
		this.isSuccess = isSuccess;
		this.result = result;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
