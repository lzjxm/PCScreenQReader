package com.cjz.tool.qr.ui;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.cjz.tool.qr.QRreader;
import com.cjz.tool.qr.config.Setting;
import com.cjz.tool.qr.ui.menu.SettingGroup;
import com.cjz.tool.qr.ui.menu.TitleMenu;
import com.cjz.tool.qr.ui.menu.TrayMenu;

import craky.component.JImagePane;
import craky.componentc.JCFrame;
import craky.layout.LineLayout;

public class MainFrame extends JCFrame {

	private static final long serialVersionUID = 2155998386881624841L;

	boolean alreadyMini2Tray = false;

	SettingGroup settingMenuGroup;

	public SettingGroup getSettingMenuGroup() {
		return settingMenuGroup;
	}

	public MainFrame() {
		super();
		settingMenuGroup = new SettingGroup(this);
		initSelf();
	}

	private void initSelf() {
		setTitle(Setting.STR_APP_TITLE);
		setIconImage(Setting.APP_ICON);
		setAlwaysOnTop(Setting.alwaysTopWhenRunning);
		setLocationRelativeTo(null);
		setSize(new Dimension(Setting.windowWidth, Setting.windowHeight));
		setLocation((Setting.screenSize.width - Setting.windowWidth) / 2, (Setting.screenSize.height - Setting.windowHeight) / 2);
		setTitleOpaque(false);
		setResizable(false);

		JPanel content = (JPanel) getContentPane();
		content.setLayout(new BorderLayout());
		content.setBorder(new EmptyBorder(10, 10, 10, 10));
		content.add(new MainPanel(this), BorderLayout.CENTER);

		initTitleMenu();

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				if (Setting.minToTray) {
					setVisible(false);
					miniTray();
				} else {
					QRreader.getInstance().exit();
				}
			}
		});
	}

	private void initTitleMenu() {
		JImagePane titleContent = getTitleContentPane();
		titleContent.setLayout(new LineLayout(0, 0, 0, 2, 0, LineLayout.TRAILING, LineLayout.LEADING, LineLayout.HORIZONTAL));
		JMenuBar titleMenu = new JMenuBar();
		titleMenu.setOpaque(false);
		titleMenu.setBorder(new EmptyBorder(0, 0, 0, 0));
		titleMenu.setBorderPainted(false);
		titleMenu.setLayout(new GridLayout(1, 1));
		titleMenu.setPreferredSize(new Dimension(20, 20));
		titleMenu.setFocusable(false);
		titleMenu.add(new TitleMenu(this));
		titleContent.add(titleMenu, LineLayout.END);
	}

	private void miniTray() {
		if (!SystemTray.isSupported()) {
			return;
		}
		SystemTray systemTray = SystemTray.getSystemTray();
		final TrayMenu trayMenu = new TrayMenu(this);

		final TrayIcon trayIcon = new TrayIcon(Setting.TRAY_ICON, Setting.STR_APP_TITLE);
		trayIcon.setImageAutoSize(true);
		trayIcon.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				maybeShowPopup(e);
			}

			private void maybeShowPopup(MouseEvent e) {
				if (e.isPopupTrigger()) {
					trayMenu.setLocation(e.getX(), e.getY());
					trayMenu.setInvoker(trayMenu);
					trayMenu.setVisible(true);
				} else if (e.getClickCount() == 2) {
					reShow();
				}
			}
		});
		try {
			if (!alreadyMini2Tray) {
				systemTray.add(trayIcon);
				alreadyMini2Tray = true;
			}
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public void reShow() {
		setVisible(true);
		setExtendedState(JFrame.NORMAL);
		toFront();
	}
}
