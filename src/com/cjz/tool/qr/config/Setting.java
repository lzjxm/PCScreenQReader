package com.cjz.tool.qr.config;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Setting {
	public static final String USER_DIR = System.getProperty("user.dir");

	public static final String CONFIG_DIR = USER_DIR + File.separator + "config";
	public static final String IMAGE_DIR = USER_DIR + File.separator + "image";
	public static final String CONFIG_PATH = CONFIG_DIR + File.separator + "config.properties";

	public static final String HIDE_SELF_WHEN_CAPTURE = "hide_self_when_capture";
	public static final String ALWAYS_TOP_WHEN_RUNNING = "hide_always_top_when_running";
	public static final String MIN_TO_TRAY = "min_to_tray";
	public static final String AUTO_COPY_TO_CLIPBOARD = "auto_copy_to_clipboard";
	public static final String STR_APP_TITLE = "屏幕QR读取器 - V1.0";

	public static final String STR_SETTING = "主菜单";
	public static final String STR_SETTING_FILE = "文件";
	public static final String STR_SETTING_FILE_COPY_RES = "复制解析结果";
	public static final String STR_SETTING_FILE_EXIT = "退出";
	public static final String STR_SETTING_ADV = "选项";
	public static final String STR_SETTING_ADV_HIDE_SELF_WHEN_CAPTURE = "截图时隐藏当前窗口";
	public static final String STR_SETTING_ADV_ALWAYS_TOP_WHEN_RUNNING = "保持窗口最前";
	public static final String STR_SETTING_ADV_MIN_TO_TRAY = "关闭时隐藏到托盘";
	public static final String STR_SETTING_ADV_AUTO_COPY_TO_SYS_CB = "解析成功后自动复制到剪切板";
	public static final String STR_SETTING_ABOUT = "关于";

	public static final String STR_SETTING_SHOW = "打开主界面";
	public static final String STR_CUT_CAPTURE_AREA = "选区截屏";
	public static final String STR_CAPTURE_FULL_AREA = "快速截屏";
	public static final String STR_COPY_RESULT = "复制结果";
	public static final String STR_ABOUT = "关于作者";

	public static final String STR_TIPS_SHOW_RESULT = "这里将展示qr解码完的结果。";
	public static final String STR_TIPS_DRAG_QR = "可直接拖放QR图片文件到此区域";
	public static final String STR_TIPS_PLZ_DRAG_QR = "请拖放一张图片!";

	public static final Image APP_ICON = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Setting.class.getClassLoader().getResource("image/app.png"))).getImage();
	public static final Image TRAY_ICON = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Setting.class.getClassLoader().getResource("image/app.png"))).getImage();
	public static final Image MENU_ICON = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Setting.class.getClassLoader().getResource("image/menu_icon.png"))).getImage();
	public static final Icon BUTTON_ICON_CAPTURE = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Setting.class.getClassLoader().getResource("image/export.png")));
	public static final Icon BUTTON_ICON_ABOUT = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Setting.class.getClassLoader().getResource("image/about.png")));
	public static final Image NORMAL_IMAGE = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Setting.class.getClassLoader().getResource("image/common_button_normal_bg.png"))).getImage();
	public static final Image ROLLOVER_IMAGE = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Setting.class.getClassLoader().getResource("image/common_button_rollover_bg.png"))).getImage();
	public static final Image PRESSED_IMAGE = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Setting.class.getClassLoader().getResource("image/common_button_pressed_bg.png"))).getImage();

	public static final int windowWidth = 520;
	public static final int windowHeight = 260;

	public static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	/**
	 * 截屏时隐藏主界面
	 */
	public static boolean hideSelfWhenCapture = true;
	/**
	 * 主界面总是置顶
	 */
	public static boolean alwaysTopWhenRunning = false;

	/**
	 * 最小化到托盘
	 */
	public static boolean minToTray = false;

	/**
	 * 自动复制到剪切板
	 */
	public static boolean autoCopyResult = false;

}
